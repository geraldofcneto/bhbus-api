var restify = require('restify'),
  mongoose = require('mongoose'),
  User = require('./user'),
  Debit = require('./debit')
  ;

mongoose.Promise = require('bluebird');

var server = restify.createServer();
server.use(restify.plugins.bodyParser({ mapParams: true }));
server.use(restify.plugins.queryParser({ mapParams: true }));
server.use(restify.plugins.authorizationParser());

User(server);
Debit(server);

var promise = mongoose.connect('mongodb://localhost:27017/bhbus', {
  useMongoClient: true
});

server.listen(8080, function () {
  console.log('%s listening at %s', server.name, server.url);
});