var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var userSchema = new mongoose.Schema({
  name: String,
  email: {
    type: String,
    unique: true
  },
  cardId: String,
  password: String
}, {
    collection: 'usercollection'
  }
);
userSchema.plugin(uniqueValidator, { message: '{PATH} ​já​ ​existente' });

module.exports = mongoose.model('User', userSchema);
