var User = require('./user.model');

module.exports = function (server) {
  server.post('/user', function (req, res, next) {
    User.create(req.body).then((user) => {
      console.log(user);
      res.send(user);
    }).catch(e => {
      console.error(e);
      res.send(e);
    });
    return next();
  })
};