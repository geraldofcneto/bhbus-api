var mongoose = require('mongoose');
var debitSchema = new mongoose.Schema({
  cardId: "string",
  code: "string",
  value: "number",
  debitedAt: { type: Date, default: Date.now }
}, {
    collection: 'debitcollection'
  }
);

module.exports = mongoose.model('debit', debitSchema);
