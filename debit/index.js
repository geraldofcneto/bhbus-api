var Debit = require('./debit.model'),
  User = require('./../user/user.model'),
  _ = require('lodash')
  ;

function checkUserPassword(user, pass, callback) {
  // Acessar serviço de autenticação
  callback(null, 'Looks good. Go on.');
}

var basicAuth = function (req, res, next) {
  res.header('WWW-Authenticate', 'Basic realm="API Docs"');

  if (
    !req.authorization ||
    !req.authorization.basic ||
    !req.authorization.basic.username ||
    !req.authorization.basic.password
  ) {
    res.send(401);
    return next(false);
  }

  checkUserPassword(req.authorization.basic.username, req.authorization.basic.password, function (err, data) {
    if (err || !data) {
      res.send(401);
      return next(false);
    }
    else return next();
  });
};

function postDebits(req, res, next) {

  User
    .count({ cardId: req.body.cardId })
    .then((exists) => {
      if (!exists)
        throw "Requisição inválida";
      return Debit.create(req.body);
    })
    .then((debit) => {
      console.log(debit);
      res.send(debit);
    })
    .catch(e => {
      console.error(e);
      res.send(e);
    });

  return next();
}

function getDebits(req, res, next) {
  console.log(req.query);

  var query = Debit.find({});

  if (req.query.cardId)
    query = query.where('cardId', req.query.cardId);

  if (req.query.initialDate && req.query.finalDate)
    query = query.where('debitedAt').gte(req.query.initialDate).lte(req.query.finalDate)

  query.exec().then(debit => {
    res.send(_.map(debit, d => {
      console.log(d);
      return { id: d._id, debitedAt: d.debitedAt, value: d.value };
    }));
  });

  return next();
}

module.exports = function (server) {
  server.post('/debit', basicAuth, postDebits);
  server.get('/debit', getDebits);
};